package rocketbot

import (
	"fmt"
	"io/ioutil"
	"log"

	"github.com/RocketChat/Rocket.Chat.Go.SDK/models"
	"github.com/RocketChat/Rocket.Chat.Go.SDK/rest"
	"github.com/robfig/cron"
)

const (
	messageStreamName = "__my_messages__"
)

type Robot struct {
	ServerURL      string
	Credentials    *models.UserCredentials
	DefaultChannel string
	ErrorChannel   string
	IgnoreUsers    []string
	CronJobs       []CronJob
	Handlers       []Handler
	OnStart        func(*Robot) error
	OnStop         func(*Robot) error
	Logger         *log.Logger
	ConnectionLog  bool
	Debug          bool

	client    *Client
	user      *models.User
	scheduler *cron.Cron
	quit      chan struct{}
}

func (bot *Robot) Client() *Client {
	return bot.client
}

func (bot *Robot) Start() error {
	if bot.Logger == nil {
		bot.Logger = log.New(ioutil.Discard, "", 0)
	}
	bot.Logger.Println("Starting rocketbot...")
	defer bot.Logger.Println("Stop rocketbot")

	client, err := NewClient(bot.ServerURL, bot.ConnectionLog, bot.Debug)
	if err != nil {
		return err
	}
	defer client.Close()

	user, err := client.Login(bot.Credentials)
	if err != nil {
		return err
	}

	// if bot.DefaultChannel != "" {
	// 	roomID, err := client.GetChannelId(bot.DefaultChannel)
	// 	if bot.Debug {
	// 		bot.Logger.Printf("Join to %s (%s)", bot.DefaultChannel, roomID)
	// 	}
	// 	if err != nil {
	// 		return err
	// 	}
	// 	if err := client.JoinChannel(roomID); err != nil {
	// 		return err
	// 	}
	// }

	bot.user = user
	bot.client = client
	bot.quit = make(chan struct{}, 1)
	defer close(bot.quit)

	if bot.OnStart != nil {
		if err := bot.OnStart(bot); err != nil {
			return err
		}
	}

	if len(bot.CronJobs) > 0 {
		bot.startScheduler()
		defer bot.stopScheduler()
	}

	channel := &models.Channel{ID: messageStreamName}
	msgCh := make(chan models.Message, 10)
	defer close(msgCh)
	if len(bot.Handlers) > 0 {
		if err := bot.client.SubscribeToMessageStream(channel, msgCh); err != nil {
			return err
		}
	}

loop:
	for {
		select {
		case <-bot.quit:
			break loop

		case msg := <-msgCh:
			bot.handleMessage(&msg)
		}
	}

	if bot.OnStop != nil {
		if err := bot.OnStop(bot); err != nil {
			return err
		}
	}

	return nil
}

func (bot *Robot) Stop() {
	bot.quit <- struct{}{}
}

func (bot *Robot) startScheduler() {
	if bot.CronJobs == nil || len(bot.CronJobs) == 0 {
		return
	}

	if bot.scheduler != nil {
		bot.scheduler.Stop()
		bot.scheduler = nil
	}

	scheduler := cron.New()
	for _, job := range bot.CronJobs {
		action := job.Run
		scheduler.AddFunc(job.JobSchedule(), func() {
			if err := action(bot); err != nil {
				bot.Logger.Println("[Robot] Job error:", err)
			}
		})
	}
	scheduler.Start()
	bot.scheduler = scheduler
	bot.Logger.Println("[Robot] Start job scheduler")
}

func (bot *Robot) stopScheduler() {
	if bot.scheduler == nil {
		return
	}

	bot.scheduler.Stop()
	bot.Logger.Println("[Robot] Stop job scheduler")
}

func (bot *Robot) SendText(channel *models.Channel, text string) (*models.Message, error) {
	if channel == nil {
		roomID, err := bot.client.GetChannelId(bot.DefaultChannel)
		if err != nil {
			return nil, err
		}
		channel = &models.Channel{
			ID: roomID,
		}
	} else if channel.ID == "" {
		roomID, err := bot.client.GetChannelId(channel.Name)
		if err != nil {
			return nil, err
		}
		channel.ID = roomID
	}

	msg := bot.client.NewMessage(channel, text)
	return bot.client.SendMessage(msg)
}

func (bot *Robot) SendMessage(msg *models.Message) (*models.Message, error) {
	return bot.client.SendMessage(msg)
}

func (bot *Robot) PostText(channelName string, text string) (*rest.MessageResponse, error) {
	if channelName == "" {
		channelName = bot.DefaultChannel
	}

	msg := &models.PostMessage{
		Channel: channelName,
		Text:    text,
	}

	return bot.PostMessage(msg)
}

func (bot *Robot) PostMessage(msg *models.PostMessage) (*rest.MessageResponse, error) {
	if msg.Channel == "" {
		msg.Channel = bot.DefaultChannel
	}
	return bot.client.PostMessage(msg)
}

func (bot *Robot) Reply(src *models.Message, text string) (*models.Message, error) {
	if src.User != nil && src.Channel != src.User.UserName {
		text = fmt.Sprintf("@%s %s", src.User.UserName, text)
	}
	channel := &models.Channel{
		ID: src.RoomID,
	}
	return bot.SendText(channel, text)
}

func (bot *Robot) handleMessage(msg *models.Message) {
	if bot.Handlers == nil || len(bot.Handlers) == 0 {
		return
	}

	if isEmptyMessage(msg) {
		return
	}

	if msg.User.ID == bot.user.ID || bot.isIgnoreUser(msg.User) {
		return
	}

	channel, err := bot.client.GetChannelInfo(msg.RoomID)
	if err != nil {
		bot.Logger.Println(err)
		return
	}

	for _, handler := range bot.Handlers {
		go bot.runHandler(msg, channel, handler)
	}
}

func (bot *Robot) runHandler(msg *models.Message, channel *models.Channel, handler Handler) {
	if !handler.Acceptable(msg, channel) {
		return
	}

	if err := handler.Run(bot, msg); err != nil {
		bot.Logger.Println("[Robot] Handler error:", err)
	}
}

func (bot *Robot) GetChannelByName(name string) (*models.Channel, error) {
	roomID, err := bot.client.GetChannelId(name)
	if err != nil {
		return nil, err
	}

	return bot.client.GetChannelInfo(roomID)
}

func (bot *Robot) GetRoomIDByName(name string) (*models.Channel, error) {
	roomID, err := bot.client.GetChannelId(name)
	if err != nil {
		return nil, err
	}

	return bot.client.GetChannelInfo(roomID)
}

func (bot *Robot) getRoomID(channel *models.Channel) (string, error) {
	if channel == nil {
		return bot.client.GetChannelId(bot.DefaultChannel)
	}

	if channel.ID != "" {
		return channel.ID, nil
	}

	return bot.client.GetChannelId(channel.Name)
}

func (bot *Robot) isIgnoreUser(user *models.User) bool {
	if user == nil || bot.IgnoreUsers == nil {
		return false
	}

	for _, iu := range bot.IgnoreUsers {
		if user.ID == iu || user.UserName == iu {
			return true
		}
	}

	return false
}

func isEmptyMessage(msg *models.Message) bool {
	return msg == nil || msg.ID == ""
}
