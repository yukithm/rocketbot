package feedwatcher

import (
	"bytes"
	"text/template"

	"github.com/yukithm/go-feedcrawler"
)

var NewItemsMarkdownTemplate = `**[{{.Feed.Title}}]({{.Feed.Link}})**

{{range .NewItems -}}
- [{{.Title}}]({{.Link}})
{{end}}`

var ErrorMarkdownTemplate = `**[{{.Subscription.ID}}]({{.Subscription.URI}})**

` + "```" + `
{{.Err.Error}}
` + "```"

var tmpl *template.Template

func initTemplate() {
	if tmpl != nil {
		return
	}
	tmpl = template.Must(template.New("newitems").Parse(NewItemsMarkdownTemplate))
	template.Must(tmpl.New("error").Parse(ErrorMarkdownTemplate))
}

type Result struct {
	feedcrawler.Result
}

func (r *Result) Markdown() (string, error) {
	initTemplate()
	var tmplName string
	if r.Err != nil {
		tmplName = "error"
	} else {
		tmplName = "newitems"
	}

	var buf bytes.Buffer
	if err := tmpl.ExecuteTemplate(&buf, tmplName, r); err != nil {
		return "", err
	}
	return buf.String(), nil
}
