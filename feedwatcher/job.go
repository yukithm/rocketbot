package feedwatcher

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"text/template"

	"github.com/RocketChat/Rocket.Chat.Go.SDK/models"
	"gitlab.com/yukithm/rocketbot"
)

type FeedJob struct {
	Schedule string
	Config   *Config

	watcher *Watcher
}

// NewFeedJob creates feed watcher job.
func NewFeedJob(schedule string, config *Config) (rocketbot.CronJob, error) {
	watcher := New(config)

	return &FeedJob{
		Schedule: schedule,
		Config:   config,
		watcher:  watcher,
	}, nil
}

func (j *FeedJob) JobSchedule() string {
	return j.Schedule
}

func (j *FeedJob) Run(bot *rocketbot.Robot) error {
	logger := bot.Logger
	if logger == nil {
		logger = log.New(ioutil.Discard, "", 0)
	}
	if bot.Debug {
		logger.Println("[FEED] Start Feed Job")
	}

	results := make([]*Result, 0)
	callback := func(result Result) {
		if result.Err != nil {
			logger.Printf("[FEED] %s: %s", result.Subscription.ID(), result.Err.Error())
			return
		} else if len(result.NewItems) == 0 {
			return
		}
		fixupLink(&result)
		results = append(results, &result)
	}

	err := j.watcher.Watch(callback)
	if err != nil {
		logger.Printf("[FEED] Error: %s", err.Error())
		return err
	}

	if len(results) > 0 {
		if err := j.publish(bot, results); err != nil {
			logger.Printf("[FEED] Error: %s", err.Error())
		}
	}

	if bot.Debug {
		logger.Println("[FEED] Finish Feed Job")
	}
	return nil
}

func (j *FeedJob) publish(bot *rocketbot.Robot, results []*Result) error {
	msg, err := j.composeMessage(results)
	if err != nil {
		return err
	}
	msg.Channel = j.Config.Channel
	_, err = bot.PostMessage(msg)

	return err
}

func (j *FeedJob) composeMessage(results []*Result) (*models.PostMessage, error) {
	contents := make([]models.Attachment, 0, len(results))
	for _, result := range results {
		content, err := j.composeAttachment(result)
		if err != nil {
			return nil, err
		}
		contents = append(contents, *content)
	}

	title := j.Config.Title
	if title == "" {
		title = "**New Feeds**"
	}

	msg := &models.PostMessage{
		Text:        title,
		Attachments: contents,
	}

	return msg, nil
}

func (j *FeedJob) composeAttachment(result *Result) (*models.Attachment, error) {
	text, err := j.composeText(result)
	if err != nil {
		return nil, err
	}
	return &models.Attachment{
		Title:     result.Feed.Title,
		TitleLink: result.Feed.Link,
		Text:      text,
	}, nil
}

var newItemsTemplate = `{{range .NewItems -}}
- [{{.Title}}]({{.Link}})
{{end}}`

func (j *FeedJob) composeText(result *Result) (string, error) {
	tmpl := template.Must(template.New("newitems").Parse(newItemsTemplate))

	var buf bytes.Buffer
	if err := tmpl.ExecuteTemplate(&buf, "newitems", result); err != nil {
		return "", err
	}
	return buf.String(), nil
}

const errorFormat = `**[ERROR]**  
` + "```" + `
%s
` + "```" + `

` + "```" + `
%s
` + "```"

func (j *FeedJob) composeErrorText(err error, res *http.Response) string {
	var body bytes.Buffer
	if res != nil && res.Body != nil {
		body.ReadFrom(res.Body)
	}

	var text string
	switch e := err.(type) {
	default:
		text = fmt.Sprintf(errorFormat, e.Error(), body.String())
	}

	return text
}

func fixupLink(result *Result) {
	if result.Feed == nil {
		return
	}

	sub, err := url.Parse(result.Subscription.URI())
	if err != nil {
		return
	}
	scheme := sub.Scheme

	if result.Feed.Link != "" {
		result.Feed.Link = fixupLinkScheme(result.Feed.Link, scheme)
	}

	if result.Feed.FeedLink != "" {
		result.Feed.FeedLink = fixupLinkScheme(result.Feed.FeedLink, scheme)
	}

	for _, item := range result.Feed.Items {
		if item.Link != "" {
			item.Link = fixupLinkScheme(item.Link, scheme)
		}
	}
}

func fixupLinkScheme(link string, scheme string) string {
	if link != "" {
		l, err := url.Parse(link)
		if err == nil && l.Scheme == "" {
			l.Scheme = scheme
			return l.String()
		}
	}
	return link
}
