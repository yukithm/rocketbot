package feedwatcher

import (
	"net/http"
	"strings"

	"github.com/yukithm/go-feedcrawler"
)

type Config struct {
	FeedsFile  string `toml:"feeds_file"`
	StatesFile string `toml:"states_file,omitempty"`
	NumWorkers int    `toml:"workers"`
	Channel    string `toml:"channel,omitempty"`
	Title      string `toml:"title,omitempty"`
}

type Watcher struct {
	feedcrawler.Crawler
	config *Config
}

func New(config *Config) *Watcher {
	crawler := feedcrawler.Crawler{
		NumWorkers: config.NumWorkers,
	}

	watcher := &Watcher{
		Crawler: crawler,
		config:  config,
	}

	return watcher
}

func (fw *Watcher) Watch(f func(Result)) error {
	if err := fw.ReloadFeeds(); err != nil {
		return err
	}
	if err := fw.ReloadStates(); err != nil {
		return err
	}

	if err := fw.CrawlFunc(func(result feedcrawler.Result) {
		f(Result{result})
	}); err != nil {
		return err
	}

	return fw.SaveStates()
}

func (fw *Watcher) ReloadFeeds() error {
	subscriptions, err := fw.loadSubscriptions()
	if err != nil {
		return err
	}
	fw.Subscriptions = subscriptions

	return nil
}

func (fw *Watcher) ReloadStates() error {
	if fw.config.StatesFile == "" {
		return nil
	}

	states, err := feedcrawler.LoadStatesFile(fw.config.StatesFile)
	if err != nil {
		return err
	}
	fw.States = states

	return nil
}

func (fw *Watcher) SaveStates() error {
	if fw.config.StatesFile == "" {
		return nil
	}

	return feedcrawler.SaveStatesFile(fw.States, fw.config.StatesFile)
}

func (fw *Watcher) loadSubscriptions() ([]feedcrawler.Subscription, error) {
	feeds, err := loadFeeds(fw.config.FeedsFile)
	if err != nil {
		return nil, err
	}

	return feedsToSubscriptions(feeds)
}

func loadFeeds(path string) ([]feedcrawler.Feed, error) {
	if strings.HasPrefix(path, "http") {
		return loadFeedsFromNetwork(path)
	}
	return feedcrawler.LoadFeedsFile(path)
}

func loadFeedsFromNetwork(url string) ([]feedcrawler.Feed, error) {
	res, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	return feedcrawler.LoadFeeds(res.Body)
}

func feedsToSubscriptions(feeds []feedcrawler.Feed) ([]feedcrawler.Subscription, error) {
	subscriptions := make([]feedcrawler.Subscription, 0, len(feeds))

	for _, feed := range feeds {
		s, err := feed.Subscription()
		if err != nil {
			return nil, err
		}
		subscriptions = append(subscriptions, s)
	}

	return subscriptions, nil
}
