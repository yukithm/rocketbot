FROM golang:alpine AS build-env

RUN apk update && apk upgrade && \
    apk add --no-cache g++ git

COPY . /go/rocketbot
WORKDIR /go/rocketbot/_example/mybot

# RUN go mod download
RUN go build -a -ldflags="-s -w"

#----------------------------------------

FROM golang:alpine
LABEL maintainer="Yuki <yukithm@gmail.com>"

# zoneinfo and certificates
# ENV ZONEINFO=/zoneinfo.zip
# ADD https://github.com/golang/go/raw/master/lib/time/zoneinfo.zip /zoneinfo.zip
# COPY --from=build-env /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt

COPY --from=build-env /go/rocketbot/_example/mybot/mybot /mybot

WORKDIR /
ENV MYBOT_CONF=./config/mybot.toml

ENTRYPOINT ["/mybot"]
