package rocketbot

// CronJob is a scheduled task.
type CronJob interface {
	// JobSchedule returns schedule pattern.
	// See https://godoc.org/github.com/robfig/cron
	// NOTE: It is different from cron, there is also seconds field.
	JobSchedule() string

	// Run job action.
	Run(*Robot) error
}

// JobFunc is job action function.
type CronJobFunc func(*Robot) error

// SimpleCronJob is a scheduled task.
type SimpleCronJob struct {
	// Schedule pattern.
	// See https://godoc.org/github.com/robfig/cron
	// NOTE: It is different from cron, there is also seconds field.
	Schedule string

	// Job function.
	Action CronJobFunc
}

func (j *SimpleCronJob) JobSchedule() string {
	return j.Schedule
}

func (j *SimpleCronJob) Run(bot *Robot) error {
	return j.Action(bot)
}
