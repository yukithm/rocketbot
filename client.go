package rocketbot

import (
	"net/url"

	"github.com/RocketChat/Rocket.Chat.Go.SDK/models"
	"github.com/RocketChat/Rocket.Chat.Go.SDK/realtime"
	"github.com/RocketChat/Rocket.Chat.Go.SDK/rest"
)

type Client struct {
	Debug    bool
	realtime *realtime.Client
	rest     *rest.Client
}

func NewClient(serverURL string, connectionLog bool, debug bool) (*Client, error) {
	url, err := url.Parse(serverURL)
	if err != nil {
		return nil, err
	}

	rtClient, err := realtime.NewClient(url, connectionLog)
	if err != nil {
		return nil, err
	}
	restClient := rest.NewClient(url, connectionLog)

	client := &Client{
		Debug:    debug,
		realtime: rtClient,
		rest:     restClient,
	}

	return client, nil
}

func (c *Client) RealtimeClient() *realtime.Client {
	return c.realtime
}

func (c *Client) RestClient() *rest.Client {
	return c.rest
}

func (c *Client) Login(credentials *models.UserCredentials) (*models.User, error) {
	if err := c.rest.Login(credentials); err != nil {
		return nil, err
	}

	user, err := c.realtime.Login(credentials)
	if err != nil {
		c.rest.Logout()
		return nil, err
	}

	return user, nil
}

func (c *Client) Logout() error {
	c.realtime.Close()
	_, err := c.rest.Logout()
	return err
}

func (c *Client) Close() error {
	return c.Logout()
}

func (c *Client) GetChannelId(name string) (string, error) {
	return c.realtime.GetChannelId(name)
}

func (c *Client) GetChannelInfo(roomID string) (*models.Channel, error) {
	return c.rest.GetChannelInfo(&models.Channel{ID: roomID})
}

func (c *Client) JoinChannel(roomID string) error {
	return c.realtime.JoinChannel(roomID)
}

func (c *Client) SubscribeToMessageStream(channel *models.Channel, msgCh chan models.Message) error {
	return c.realtime.SubscribeToMessageStream(channel, msgCh)
}

func (c *Client) NewMessage(channel *models.Channel, text string) *models.Message {
	return c.realtime.NewMessage(channel, text)
}

func (c *Client) SendMessage(msg *models.Message) (*models.Message, error) {
	return c.realtime.SendMessage(msg)
}

func (c *Client) PostMessage(msg *models.PostMessage) (*rest.MessageResponse, error) {
	return c.rest.PostMessage(msg)
}
