module gitlab.com/yukithm/rocketbot

go 1.12

require (
	github.com/Jeffail/gabs v1.1.0 // indirect
	github.com/PuerkitoBio/goquery v1.5.0 // indirect
	github.com/RocketChat/Rocket.Chat.Go.SDK v0.0.0-20190424183144-888c69384c89
	github.com/gopackage/ddp v0.0.0-20170117053602-652027933df4 // indirect
	github.com/kylelemons/godebug v1.1.0 // indirect
	github.com/mmcdole/gofeed v1.0.0-beta2 // indirect
	github.com/mmcdole/goxpp v0.0.0-20181012175147-0068e33feabf // indirect
	github.com/naoina/go-stringutil v0.1.0 // indirect
	github.com/naoina/toml v0.1.1 // indirect
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/robfig/cron v1.1.0
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/yukithm/go-feedcrawler v2.2.0+incompatible
	golang.org/x/net v0.0.0-20190603091049-60506f45cf65 // indirect
	golang.org/x/sync v0.0.0-20190423024810-112230192c58 // indirect
	golang.org/x/sys v0.0.0-20190602015325-4c4f7f33c9ed // indirect
	golang.org/x/text v0.3.2 // indirect
)
