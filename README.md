# rocketbot

rocketbot is a chat bot framework for Rocket.Chat.

- Support message handler.
- Support cron style job.
- Including feeds (RSS, Atom, etc) job.
- Including example bot implementation in `_example/mybot`.

## Using Docker (for example bot)

```bash
# copy and edit configuration files
cp ./_example/mybot/config/mybot.toml.example ./_example/mybot/config/mybot.toml
cp ./_example/mybot/config/feeds.toml.example ./_example/mybot/config/feeds.toml
$EDITOR ./_example/mybot/config/mybot.toml
$EDITOR ./_example/mybot/config/feeds.toml

# build and run example bot
docker-compose build
docker-compose run mybot
```

## Build your own bot

1. `git init /path/to/your_bot`
2. Copy `_example/mybot/*` into your repository.
3. Edit `handlers/*.go` and `jobs/*.go`  
   Or customize all of codes.

## License

MIT

## Author

yukithm
