package main

import (
	"flag"
	"io"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/RocketChat/Rocket.Chat.Go.SDK/models"
	"gitlab.com/yukithm/rocketbot"
	"gitlab.com/yukithm/rocketbot/_example/mybot/app"
	"gitlab.com/yukithm/rocketbot/_example/mybot/handlers"
	"gitlab.com/yukithm/rocketbot/_example/mybot/jobs"
	"gitlab.com/yukithm/rocketbot/_example/mybot/utils"
)

const (
	defaultConfigFile = "./config/mybot.toml"
)

var (
	configfile    = flag.String("conf", utils.FirstString(os.Getenv("MYBOT_CONF"), defaultConfigFile), "config file")
	logfile       = flag.String("log", "", "log file")
	pidfile       = flag.String("pid", "", "pid file")
	debug         = flag.Bool("debug", false, "debug mode")
	overrideDebug = false
)

func main() {
	code := _main()
	if code != 0 {
		os.Exit(code)
	}
}

func _main() int {
	flag.Parse()
	flag.Visit(func(f *flag.Flag) {
		if f.Name == "debug" {
			overrideDebug = true
		}
	})

	cfg, err := app.LoadConfigFile(*configfile)
	if err != nil {
		panic(err)
	}
	if overrideDebug {
		cfg.Debug = *debug
	}

	logdev := getLogDevice(utils.FirstString(*logfile, cfg.LogFile))
	if wc, ok := logdev.(io.Closer); ok {
		defer wc.Close()
	}
	logger := log.New(logdev, "", log.LstdFlags)
	log.SetOutput(logdev) // workaround

	if *pidfile != "" {
		pid, err := createPID(*pidfile)
		if err != nil {
			log.Println(err)
			return 1
		}
		defer pid.Remove()
	}

	if err := runRobot(cfg, logger); err != nil {
		logger.Print(err)
		return 1
	}

	return 0
}

func runRobot(cfg *app.Config, logger *log.Logger) error {
	cronJobs, err := jobs.Build(cfg, logger)
	if err != nil {
		return err
	}
	handlers, err := handlers.Build(cfg, logger)
	if err != nil {
		return err
	}

	bot := &rocketbot.Robot{
		ServerURL: cfg.ServerURL,
		Credentials: &models.UserCredentials{
			Email:    cfg.UserID,
			Password: cfg.Password,
		},
		DefaultChannel: cfg.DefaultChannel,
		IgnoreUsers:    cfg.IgnoreUsers,
		ConnectionLog:  cfg.ConnectionLog,
		Debug:          cfg.Debug,
		Logger:         logger,
		CronJobs:       cronJobs,
		Handlers:       handlers,
		OnStart:        jobs.Events.OnStart,
		OnStop:         jobs.Events.OnStop,
	}

	sigCh := make(chan os.Signal)
	signal.Notify(sigCh,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)

	go func() {
		s := <-sigCh
		logger.Printf("%q received", s)
		bot.Stop()
	}()

	if err := bot.Start(); err != nil {
		return err
	}

	return nil
}

func getLogDevice(name string) io.Writer {
	switch name {
	case "":
		return os.Stderr

	case "-":
		return os.Stdout

	default:
		w, err := os.OpenFile(name, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0600)
		if err != nil {
			panic(err)
		}
		return w
	}
}

func createPID(name string) (*app.PIDFile, error) {
	pid, err := app.NewPIDFile(name)
	if err != nil {
		return nil, err
	}
	if err := pid.Create(); err != nil {
		return nil, err
	}
	return &pid, nil
}
