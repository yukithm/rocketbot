package app

import (
	"io/ioutil"
	"os"
	"strconv"

	"gitlab.com/yukithm/rocketbot/_example/mybot/utils"
)

type PIDFile string

func NewPIDFile(pidfile string) (PIDFile, error) {
	if pidfile == "" {
		return "", nil
	}

	pidfile, err := utils.AbsPath(pidfile)
	if err != nil {
		return "", err
	}

	return PIDFile(pidfile), nil
}

func (p PIDFile) Exists() bool {
	if p == "" {
		return false
	}
	return utils.FileExists(string(p))
}

func (p PIDFile) Create() error {
	if p == "" {
		return nil
	}

	pid := strconv.Itoa(os.Getpid())
	return ioutil.WriteFile(string(p), []byte(pid), 0644)
}

func (p PIDFile) Remove() error {
	if p == "" {
		return nil
	}

	err := os.Remove(string(p))
	if err != nil && !os.IsNotExist(err) {
		return err
	}
	return nil
}
