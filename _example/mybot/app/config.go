package app

import (
	"io/ioutil"
	"os"

	"github.com/naoina/toml"
	"gitlab.com/yukithm/rocketbot/feedwatcher"
)

const (
	serverURLEnv = "ROCKETCHAT_URL"
	userIDEnv    = "ROCKETCHAT_USER"
	passwordEnv  = "ROCKETCHAT_PASSWORD"
	channelEnv   = "ROCKETCHAT_ROOM"
)

// Config is a configuration for mybot.
type Config struct {
	ServerURL      string       `toml:"server_url"`
	UserID         string       `toml:"user_id"`
	Password       string       `toml:"password"`
	DefaultChannel string       `toml:"default_channel"`
	IgnoreUsers    []string     `toml:"ignore_users"`
	LogFile        string       `toml:"log_file,omitempty"`
	PidFile        string       `toml:"pid_file,omitempty"`
	ConnectionLog  bool         `toml:"connection_log"`
	Debug          bool         `toml:"debug"`
	Feeds          *FeedsConfig `toml:"feeds"`
}

// FeedsConfig is a configuration for feedcrawler.
type FeedsConfig struct {
	FeedsFile  string `toml:"feeds_file,omitempty"`
	StatesFile string `toml:"states_file,omitempty"`
	NumWorkers int    `toml:"workers,omitempty"`
	Channel    string `toml:"channel,omitempty"`
	Title      string `toml:"title,omitempty"`
	Schedule   string `toml:"schedule"`
}

func (fc *FeedsConfig) WatcherConfig() *feedwatcher.Config {
	return &feedwatcher.Config{
		FeedsFile:  fc.FeedsFile,
		StatesFile: fc.StatesFile,
		NumWorkers: fc.NumWorkers,
		Channel:    fc.Channel,
		Title:      fc.Title,
	}
}

func LoadConfigFile(file string) (*Config, error) {
	var cfg Config
	buf, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	err = toml.Unmarshal(buf, &cfg)
	if err != nil {
		return nil, err
	}

	setEnvValue(&cfg.ServerURL, serverURLEnv)
	setEnvValue(&cfg.UserID, userIDEnv)
	setEnvValue(&cfg.Password, passwordEnv)
	setEnvValue(&cfg.DefaultChannel, channelEnv)

	// if cfg.ServerURL == "" {
	// 	return nil, fmt.Errorf("ConfigError: server_url is required in %s", file)
	// }
	//
	// if cfg.UserID == "" {
	// 	return nil, fmt.Errorf("ConfigError: user_id is required in %s", file)
	// }
	//
	// if cfg.Password == "" {
	// 	return nil, fmt.Errorf("ConfigError: password is required in %s", file)
	// }

	return &cfg, nil
}

func setEnvValue(value *string, envName string) {
	envValue := os.Getenv(envName)
	if envValue != "" {
		*value = envValue
	}
}
