package jobs

import "gitlab.com/yukithm/rocketbot"

func init() {
	Events.OnStop = func(bot *rocketbot.Robot) error {
		bot.PostText("bot", "Bot shutdown.")
		return nil
	}
}
