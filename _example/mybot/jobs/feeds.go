package jobs

import (
	"log"

	"gitlab.com/yukithm/rocketbot"
	"gitlab.com/yukithm/rocketbot/_example/mybot/app"
	"gitlab.com/yukithm/rocketbot/feedwatcher"
)

func init() {
	Register(func(cfg *app.Config, logger *log.Logger) (rocketbot.CronJob, error) {
		return feedwatcher.NewFeedJob(cfg.Feeds.Schedule, cfg.Feeds.WatcherConfig())
	})
}
