package jobs

import (
	"log"
	"time"

	"gitlab.com/yukithm/rocketbot"
	"gitlab.com/yukithm/rocketbot/_example/mybot/app"
)

func init() {
	Register(func(cfg *app.Config, logger *log.Logger) (rocketbot.CronJob, error) {
		return &rocketbot.SimpleCronJob{
			Schedule: "0 * * * * *",
			Action: func(bot *rocketbot.Robot) error {
				text := time.Now().Format("15時4分です")
				bot.PostText("", text)
				return nil
			},
		}, nil
	})
}
