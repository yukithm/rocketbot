package jobs

import "gitlab.com/yukithm/rocketbot"

func init() {
	Events.OnStart = func(bot *rocketbot.Robot) error {
		bot.PostText("bot", "Bot booted.")
		return nil
	}
}
