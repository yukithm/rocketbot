package jobs

import (
	"log"

	"gitlab.com/yukithm/rocketbot"
	"gitlab.com/yukithm/rocketbot/_example/mybot/app"
)

type CronJobFactory func(cfg *app.Config, logger *log.Logger) (rocketbot.CronJob, error)

type CronJobsBuilder struct {
	factories []CronJobFactory
}

func (b *CronJobsBuilder) Register(factory CronJobFactory) {
	if b.factories == nil {
		b.factories = make([]CronJobFactory, 0)
	}
	b.factories = append(b.factories, factory)
}

func (b *CronJobsBuilder) Build(cfg *app.Config, logger *log.Logger) ([]rocketbot.CronJob, error) {
	if b.factories == nil {
		return make([]rocketbot.CronJob, 0), nil
	}

	jobs := make([]rocketbot.CronJob, 0, len(b.factories))

	for _, factory := range b.factories {
		job, err := factory(cfg, logger)
		if err != nil {
			return nil, err
		}
		jobs = append(jobs, job)
	}

	return jobs, nil
}

var CronJobs = &CronJobsBuilder{}

func Register(factory CronJobFactory) {
	CronJobs.Register(factory)
}

func Build(cfg *app.Config, logger *log.Logger) ([]rocketbot.CronJob, error) {
	return CronJobs.Build(cfg, logger)
}

type EventJob func(*rocketbot.Robot) error

type EventJobs struct {
	OnStart EventJob
	OnStop  EventJob
}

var Events = &EventJobs{}
