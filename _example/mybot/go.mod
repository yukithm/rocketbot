module gitlab.com/yukithm/rocketbot/_example/mybot

go 1.12

require (
	github.com/Jeffail/gabs v1.4.0 // indirect
	github.com/RocketChat/Rocket.Chat.Go.SDK v0.0.0-20190424183144-888c69384c89
	github.com/naoina/toml v0.1.1
	gitlab.com/yukithm/rocketbot v0.5.3
)

replace gitlab.com/yukithm/rocketbot => ../../
