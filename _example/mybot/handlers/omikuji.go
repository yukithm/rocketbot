package handlers

import (
	"log"
	"regexp"

	"github.com/RocketChat/Rocket.Chat.Go.SDK/models"
	"gitlab.com/yukithm/rocketbot"
	"gitlab.com/yukithm/rocketbot/_example/mybot/app"
)

type Omikuji struct {
	Filter     *rocketbot.HandlerFilter
	Candidates []string
}

func (o *Omikuji) Acceptable(msg *models.Message, channel *models.Channel) bool {
	return o.Filter.Match(msg, channel)
}

func (o *Omikuji) Run(bot *rocketbot.Robot, msg *models.Message) error {
	result := o.Get(msg.User.UserName)
	bot.Reply(msg, result)
	return nil
}

func (o *Omikuji) Get(name string) string {
	n := 0
	for _, c := range name {
		n += int(c)
	}

	index := n % len(o.Candidates)
	return o.Candidates[index]
}

func init() {
	Register(omikujiHandler)
}

func omikujiHandler(cfg *app.Config, logger *log.Logger) (rocketbot.Handler, error) {
	return &Omikuji{
		Filter: &rocketbot.HandlerFilter{
			Message: regexp.MustCompile(`おみくじ`),
		},
		Candidates: []string{
			"大吉",
			"中吉",
			"小吉",
			"吉",
			"半吉",
			"末吉",
			"凶",
			"小凶",
			"大凶",
		},
	}, nil
}
