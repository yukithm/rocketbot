package handlers

import (
	"log"
	"regexp"

	"github.com/RocketChat/Rocket.Chat.Go.SDK/models"
	"gitlab.com/yukithm/rocketbot"
	"gitlab.com/yukithm/rocketbot/_example/mybot/app"
)

func init() {
	Register(helloHandler)
}

func helloHandler(cfg *app.Config, logger *log.Logger) (rocketbot.Handler, error) {
	return &rocketbot.SimpleHandler{
		Filter: &rocketbot.HandlerFilter{
			Message: regexp.MustCompile(`(?i:hello|こんにちは)`),
		},
		Action: func(bot *rocketbot.Robot, msg *models.Message) error {
			bot.Reply(msg, "こんにちは！")
			return nil
		},
	}, nil
}
