package handlers

import (
	"log"

	"gitlab.com/yukithm/rocketbot"
	"gitlab.com/yukithm/rocketbot/_example/mybot/app"
)

type HandlerFactory func(cfg *app.Config, logger *log.Logger) (rocketbot.Handler, error)

type HandlersBuilder struct {
	factories []HandlerFactory
}

func (b *HandlersBuilder) Register(factory HandlerFactory) {
	if b.factories == nil {
		b.factories = make([]HandlerFactory, 0)
	}
	b.factories = append(b.factories, factory)
}

func (b *HandlersBuilder) Build(cfg *app.Config, logger *log.Logger) ([]rocketbot.Handler, error) {
	if b.factories == nil {
		return make([]rocketbot.Handler, 0), nil
	}

	handlers := make([]rocketbot.Handler, 0, len(b.factories))

	for _, factory := range b.factories {
		handler, err := factory(cfg, logger)
		if err != nil {
			return nil, err
		}
		handlers = append(handlers, handler)
	}

	return handlers, nil
}

var Handlers = &HandlersBuilder{}

func Register(factory HandlerFactory) {
	Handlers.Register(factory)
}

func Build(cfg *app.Config, logger *log.Logger) ([]rocketbot.Handler, error) {
	return Handlers.Build(cfg, logger)
}
