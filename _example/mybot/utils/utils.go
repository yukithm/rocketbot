package utils

import (
	"os"
	"path/filepath"
)

func FirstString(str ...string) string {
	for _, s := range str {
		if s != "" {
			return s
		}
	}

	return ""
}

func AbsPath(file string) (string, error) {
	if file == "" {
		return "", nil
	}

	if !filepath.IsAbs(file) {
		exec, err := os.Executable()
		if err != nil {
			return "", err
		}
		file = filepath.Join(filepath.Dir(exec), file)
	}

	return file, nil
}

func FileExists(file string) bool {
	if _, err := os.Stat(file); err != nil {
		return false
	}
	return true
}
