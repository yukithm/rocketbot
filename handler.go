package rocketbot

import (
	"regexp"

	"github.com/RocketChat/Rocket.Chat.Go.SDK/models"
)

// Handler is a message handler.
type Handler interface {
	Acceptable(*models.Message, *models.Channel) bool
	Run(*Robot, *models.Message) error
}

type HandlerFilter struct {
	Channel  *regexp.Regexp
	UserName *regexp.Regexp
	Message  *regexp.Regexp
}

func (f *HandlerFilter) Match(msg *models.Message, channel *models.Channel) bool {
	return f.MatchChannel(channel) && f.MatchUser(msg.User) && f.MatchMessage(msg)
}

func (f *HandlerFilter) MatchChannel(channel *models.Channel) bool {
	if f.Channel == nil {
		return true
	}

	if channel == nil {
		return false
	}

	return f.Channel.MatchString(channel.Name)
}

func (f *HandlerFilter) MatchUser(user *models.User) bool {
	if f.UserName == nil {
		return true
	}

	if user == nil {
		return false
	}

	return f.UserName.MatchString(user.UserName)
}

func (f *HandlerFilter) MatchMessage(msg *models.Message) bool {
	if f.Message == nil {
		return true
	}

	if msg == nil {
		return false
	}

	return f.Message.MatchString(msg.Msg)
}

type HandlerFunc func(*Robot, *models.Message) error

type SimpleHandler struct {
	Filter *HandlerFilter
	Action HandlerFunc
}

func (h *SimpleHandler) Acceptable(msg *models.Message, channel *models.Channel) bool {
	return h.Filter == nil || h.Filter.Match(msg, channel)
}

func (h *SimpleHandler) Run(bot *Robot, msg *models.Message) error {
	return h.Action(bot, msg)
}
